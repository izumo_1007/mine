require 'io/console'

@b = 0
@c = 0
@d = 0
@e = 0
@number = []
def random
    b = rand(@s)
    c = rand(@s)
    if @a[b][c][1] == "B"
        random
    else
        @a[b][c][1] = "B"
    end
end

# 盤面の作成
def mine_new
    @a = Array.new(@s + 1).map{Array.new(@s + 1).map{Array.new([0,0])}}
    # 地雷の追加
    @s.times do
        random
    end
    for i in 0..@s - 1 do
        for j in 0..@s - 1 do
            if @a[i][j][1] != "B"
              unless @b == i && @c == j
                if @a[i - 1][j - 1][1] == "B"
                    @a[i][j][1] += 1
                end
              end
                if @a[i - 1][j][1] == "B"
                    @a[i][j][1] += 1
                end
                if @a[i - 1][j + 1][1] == "B"
                    @a[i][j][1] += 1
                end
                if @a[i][j - 1][1] == "B"
                    @a[i][j][1] += 1
                end
                if @a[i][j + 1][1] == "B"
                    @a[i][j][1] += 1
                end
                if @a[i + 1][j - 1][1] == "B"
                    @a[i][j][1] += 1
                end
                if @a[i + 1][j][1] == "B"
                    @a[i][j][1] += 1
                end
                if @a[i + 1][j + 1][1] == "B"
                    @a[i][j][1] += 1
                end
                if @a[i][j][1] == 0
                    @a[i][j][1] = "."
                end
            end
        end
    end
end

# 盤面をコンソール上にprint
def mine_print

    for i in 0..@s - 1 do
        for j in 0..@s - 1 do
            if @a[i][j][2] == "p"
              print @a[i][j][2]
            elsif @a[i][j][0] == 0 || @a[i][j][0] == "f"
                print @a[i][j][0]
            else
                print @a[i][j][1]
            end
        end
        print "\n"
    end
end

#地雷が埋まってないマスを開ける
@visited = []
@r = []

def open(x,y)
    i = 0
    # puts x.to_s + " " + y.to_s
    if x > 1
        if @a[x - 1][y][1] == "." && !(@visited.include?([x - 1,y]))
            # @a[x - 1][y][0] = 1
            i += 1
            m = "left"
        end
    end

    if x <= @s
        if @a[x + 1][y][1] == "." && !(@visited.include?([x + 1,y]))
            # @a[x + 1][y][0] = 1
            i += 1
            m = "right"
        end
    end

    if y > 1
        if @a[x][y - 1][1] == "."  && !(@visited.include?([x,y - 1]))
            # @a[x][y - 1][0] = 1
            i += 1
            m = "up"
        end
    end

    if y <= @s
        if @a[x][y + 1][1] == "."  && !(@visited.include?([x,y + 1]))
            # @a[x][y + 1][0] = 1
            i += 1
            m = "down"
        end
    end

    if 0 < x
        @a[x - 1][y][0] = "1"
    end
    if x <= @s
        @a[x + 1][y][0] = "1"
    end
    if 0 < y
        @a[x][y - 1][0] = "1"
    end
    if y <= @s
        @a[x][y + 1][0] = "1"
    end
    if i >= 2
        @r.push([x,y])
    end

    if m == "up"
        @visited.push([x,y - 1])
        open(x,y - 1)
    end
    if m == "down"
        @visited.push([x,y + 1])
        open(x,y + 1)
    end
    if m == "left"
        @visited.push([x - 1,y])
        open(x - 1,y)
    end
    if m == "right"
        @visited.push([x + 1,y])
        open(x + 1,y)
    end
    return if @r.empty?
    x = @r[0][0]
    y = @r[0][1]
    @r.delete([x,y])
    open(x,y)
end

# 開ける場所を指定
def play
    @a[@b][@c][0] = "1"
    if @a[@b][@c][1] == "."
        open(@b,@c)
    end
end

# 旗を立てる
def flag
    if @a[@b][@c][0] != "f"
        @a[@b][@c][0] = "f"
    else
        @a[@b][@c][0] = 0
    end
end



puts "\e[H\e[2J"
puts "10~を入力してください"
puts "pの位置をenterで開けます"
puts "pの位置にfでflagを立てられます"
@s = gets.to_i
mine_new
@n = [0,0,nil]
@b = @n[0]
@c = @n[1]
@a[@b][@c][2] = "p"
puts "\e[H\e[2J"
mine_print
while (key = STDIN.getch) != "\C-c"
  if key.inspect == '"A"' && @b > 0
    @a[@b][@c][2] = nil
    @b -= 1
    @a[@b][@c][2] = "p"
  elsif key.inspect == '"B"' && @b < @s - 1
    @a[@b][@c][2] = nil
    @b += 1
    @a[@b][@c][2] = "p"
  elsif key.inspect == '"C"' && @c < @s - 1
    @a[@b][@c][2] = nil
    @c += 1
    @a[@b][@c][2] = "p"
  elsif key.inspect == '"D"' && @c > 0
    @a[@b][@c][2] = nil
    @c -= 1
    @a[@b][@c][2] = "p"

    # 指定した場所が地雷かどうか判定
    elsif key.inspect == '"\r"'
            while @a[@b][@c][1] == "B"
              mine_new
            end
            play
            puts "\e[H\e[2J"
            mine_print
            break
    elsif key.inspect == '"f"'
        flag
  end
  puts "\e[H\e[2J"
  mine_print
end

    while (key = STDIN.getch) != "\C-c"
      if key.inspect == '"A"' && @b > 0
        @a[@b][@c][2] = nil
        @b -= 1
        @a[@b][@c][2] = "p"
      elsif key.inspect == '"B"' && @b < @s - 1
        @a[@b][@c][2] = nil
        @b += 1
        @a[@b][@c][2] = "p"
      elsif key.inspect == '"C"' && @c < @s - 1
        @a[@b][@c][2] = nil
        @c += 1
        @a[@b][@c][2] = "p"
      elsif key.inspect == '"D"' && @c > 0
        @a[@b][@c][2] = nil
        @c -= 1
        @a[@b][@c][2] = "p"

        # 指定した場所が地雷かどうか判定
        elsif key.inspect == '"\r"'

                play
                if @a[@b][@c][1] == "B"
                    puts "\e[H\e[2J"
                    for i in 0..@s - 1 do
                        for j in 0..@s - 1 do
                            print @a[i][j][1]
                        end
                        print "\n"
                    end
                    puts "GAME OVER"
                    break
                end
        elsif key.inspect == '"f"'
            flag
      end
        puts "\e[H\e[2J"
        mine_print
        bomb = 0
        # クリア判定
        for i in 0..@s - 1 do
            for j in 0..@s - 1 do
                if @a[i][j][0] == 0 || @a[i][j][0] == "f"
                    bomb += 1
                end
                if bomb > @s
                    break
                end
            end
            if bomb > @s
                    break
            end
        end
        if bomb == @s
            puts "CREAR"
            break
        end
    end
